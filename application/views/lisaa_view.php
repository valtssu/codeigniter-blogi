
<form method="post" action="<?php print site_url(); ?>/posts/tallenna">
    <div class="form-group">
        <label>Otsikko</label>
        <input type="hidden" name="id" value='<?php print $this->session->id ?>'>
        <input type="text" style="width: 25%" name="otsikko">
    </div>
    <div class="form-group">
        <label>Teksti</label>
        <?php
        $data = array(
            'name' => 'teksti',
            'id' => 'teksti',
            'value' => '',
            'rows' => '5',
            'cols' => '5',
            'style' => 'width:25%',
        );

        echo form_textarea($data);
        ?> 


    </div>   
    <button type="submit" class="btn btn-default">Tallenna</button>

</form>
