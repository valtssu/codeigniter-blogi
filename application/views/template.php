<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

    <title>Blogi</title>

    <link href="<?php echo base_url() . "assets/css/bootstrap.min.css" ?>" rel="stylesheet">
    <link href="<?php echo base_url() . "assets/css/starter-template.css"; ?>" rel="stylesheet">
    
  </head>

  <body>


    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
                    <a class="navbar-brand" href="#">Blogi</a>
        </div>
          
          
        
          <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url(); ?>">Etusivulle</a></li>
            <li><?php (isset($this->session->logged_in)?print anchor("posts/lisaa", "Lisää kirjoitus"):print ""); ?></li>
            <li><?php (isset($this->session->logged_in)?print anchor("login/logout", "Kirjaudu ulos"):print anchor("login/index", "Kirjaudu sisään")); ?></li>
            <li><?php (!isset($this->session->logged_in)?print anchor("login/rekisterointi", "Rekisteröidy"):print ""); ?></li>
          </ul>
        
      </div>
    </nav>

    <div class="container">

      <div class="starter-template">
        <div class="col-lg-12">
            <?php
            $this->load->view($main_content);
            ?>
        </div>
      </div>

    </div>
    <script src="<?php echo base_url() . "assets/js/jquery.min.js"; ?>"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   
</body></html>

