<?php

class posts_model extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function hae_kaikki(){
        $this->db->select('*,kirjoitus.id as `kirjoitus_id`');
        $this->db->from('kirjoitus');
        $this->db->join('kayttaja', 'kayttaja.id = kirjoitus.kayttaja_id');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function poista($id){
        
        
        $this->db->where('kirjoitus_id', $id);
        $this->db->delete('kommentti');
        
        $this->db->where('id', $id);
        $this->db->delete('kirjoitus');
        
    }
    
    public function hae($id){
        $this->db->select('*,kirjoitus.id as `kirjoitus_id`');
        $this->db->from('kirjoitus');
        $this->db->join('kayttaja', 'kayttaja.id = kirjoitus.kayttaja_id');
        $this->db->where('kirjoitus.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function lisaa($data){
        $this->db->insert('kirjoitus', $data);
        return $this->db->insert_id();
    }
}
