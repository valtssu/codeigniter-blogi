<?php
class comment_model extends CI_Model {
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function lisaa($data){
        $this->db->insert('kommentti', $data);
        return $this->db->insert_id();
    }
    
    public function hae_kommentit($kirjoitus_id){
        $this->db->from('kommentti');
        $this->db->join('kayttaja', 'kayttaja.id = kommentti.kayttaja_id');
        $this->db->where('kirjoitus_id', $kirjoitus_id);
        $query = $this->db->get();
        return $query->result();
    }
}
