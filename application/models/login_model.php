<?php

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login($data) {
        $condition = "tunnus =" . "'" . $data['username'] . "' AND " . "salasana =" . "'" . $data['password'] . "'";
        $this->db->select('*');
        $this->db->from('kayttaja');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
    
    public function rekisteroidy($data){
        $this->db->insert('kayttaja', $data);
        return $this->db->insert_id();
    }

}
