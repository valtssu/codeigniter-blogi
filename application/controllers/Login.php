<?php

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index() {
        $data['main_content'] = 'kirjaudu_view';
        $this->load->view('template.php', $data);
    }

    public function rekisterointi() {
        $data['main_content'] = 'rekisteroidy_view';
        $this->load->view('template.php', $data);
    }

    public function rekisteroidy() {
        $data = array(
            'etunimi' => $this->input->post('etunimi'),
            'sukunimi' => $this->input->post('sukunimi'),
            'tunnus' => $this->input->post('username'),
            'salasana' => md5($this->input->post("password"))
        );
        $this->login_model->rekisteroidy($data);
        redirect('posts/index', 'refresh');
    }

    public function kirjaudu() {
        $data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('salasana'))
        );
        $id = $this->login_model->login($data);
        if ($id) {
            $this->session->set_userdata('id', $id);
            $this->session->set_userdata('logged_in', TRUE);
            $this->session->set_userdata('username', $data['username']);

            redirect('posts/index', 'refresh');
        } else {
            print("Väärä tunnus tai salasana");
        }
    }

    public function logout() {
        session_destroy();
        redirect('posts/index', 'refresh');
    }

}
