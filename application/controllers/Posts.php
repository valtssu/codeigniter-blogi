<?php

class Posts extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('posts_model');
        $this->load->model('comment_model');
        
    }
    
    public function index(){
        $data['kirjoitukset']=$this->posts_model->hae_kaikki();
        $data['main_content']='kirjoitukset_view';
        
        $this->load->view('template.php', $data);
    }
    
    public function poista($id){
        $this->posts_model->poista($id);
        redirect('posts/index', 'refresh');
    }
    
    public function nayta($kirjoitus_id){
        
        $data['kirjoitus']=$this->posts_model->hae($kirjoitus_id);
        $data['kommentit']=$this->comment_model->hae_kommentit($kirjoitus_id);
        $data['main_content']='kirjoitus_view';
        $this->load->view('template.php', $data);
    }
    
    public function lisaa(){
       $data['main_content'] = 'lisaa_view';
       $this->load->view('template.php', $data);
    }
    
    public function lisaa_kommentti(){
        $data = array(
           'teksti'        =>$this->input->post('kommentti'),
           'kirjoitus_id'         =>$this->input->post('kirjoitus_id'),
           'kayttaja_id'    =>$this->input->post('kayttaja_id')
       );
       
       $this->comment_model->lisaa($data);
       redirect('posts/index', 'refresh');
    }
    
    
    public function tallenna(){
        $data = array(
           'otsikko'        =>$this->input->post('otsikko'),
           'teksti'         =>$this->input->post('teksti'),
           'kayttaja_id'    =>$this->input->post('id')
       );
       $this->posts_model->lisaa($data);
       redirect('posts/index', 'refresh');
    }
}
